# gnome-11pm-notificator
Send notification before auto suspend at 11:00 PM

## Perequisites
```
go get github.com/0xAX/notificator
```

## Build
```
cd $GOPATH/src/github.com
git clone github.com/okcy1016/gnome-11pm-notificator
cd $GOPATH/src/github.com/gnome-11pm-notificator/
go build
```

## Specical thanks
Notificator golang library from [0xAX](https://github.com/0xAX/notificator)
