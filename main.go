package main

import (
	"github.com/0xAX/notificator"
	"log"
)

var notify *notificator.Notificator

func main() {
	notify = notificator.New(notificator.Options{
		DefaultIcon: "/home/phantom/workplaces/go/src/github.com/0xAX/notificator/icon/golang.png",
		AppName:     "Gnome 11PM notificator",
	})

	// check errors
	err := notify.Push("Warning!!!", "1 minute left to 11:00 PM! Almost execute suspend.", "/home/phantom/workplaces/go/src/github.com/0xAX/notificator/icon/golang.png", notificator.UR_CRITICAL)

	if err != nil {
		log.Fatal(err)
	}
}
